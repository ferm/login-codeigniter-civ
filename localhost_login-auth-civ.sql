--
-- Database: `login-auth-civ`
--
DROP DATABASE IF EXISTS login-auth-civ;
CREATE DATABASE IF NOT EXISTS login-auth-civ DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE login-auth-civ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users(
	id_user int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	act_user int(4) NOT NULL DEFAULT '1',
	firstname varchar(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
	lastname varchar(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
	email varchar(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
	password varchar(255) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
	created_at datetime NOT NULL DEFAULT current_timestamp(),
	updated_at datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

DROP TABLE IF EXISTS resets;
CREATE TABLE IF NOT EXISTS resets(
	id_res int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	fc_res timestamp NOT NULL DEFAULT current_timestamp(),
	uuid varchar(255) DEFAULT NULL,
	fe_res varchar(523) DEFAULT NULL,
	edo_res int(11) DEFAULT 0,
	ip_res varchar(40) DEFAULT '',
	email varchar(255) DEFAULT '',
	user_id int(11) DEFAULT NULL,
	foreign key(user_id) references users(id_user) on update cascade on delete cascade
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;