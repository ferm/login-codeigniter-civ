<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-sm-8 col-md-6 mt-5 pt-3 pb-3 bg-white from-wrapper border border-dark rounded">
			<h3>Inicio de sesión</h3>
			<hr>
			<?php if (session()->get('success')): ?>
			<div class="alert alert-success" role="alert">
			<?= session()->get('success') ?>
			</div>
			<?php endif; ?>
			<?php if (isset($validation)): ?>
			<div class="alert alert-danger" role="alert">
			<?= $validation->listErrors() ?>
			</div>
			<?php endif; ?>
			<form class="" action="<?=base_url('login');?>" method="post">
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" class="form-control" name="email" id="email" value="<?=set_value('email');?>" required="" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="password">Contraseña</label>
					<input type="password" class="form-control" name="password" id="password" value="" required="">
				</div>
				<div class="row">
					<div class="col-12 col-sm-8">
						<button type="submit" class="btn btn-primary">Continuar</button>
						<a href="<?=base_url('validate');?>">Olvidaste contraseña?</a>
					</div>
					<div class="col-12 col-sm-4 text-right">
						<a href="<?=base_url('register');?>">No tienes cuenta?</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
