<div class="container mt-3">
	<div class="row justify-content-center">
		<div class="col-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
					<li class="breadcrumb-item active" aria-current="page">Perfil</li>
				</ol>
			</nav>
		</div>

		<div class="col-12 col-sm-8 col-md-6 mt-5 pt-3 pb-3 bg-white from-wrapper border rounded border-dark">
			<h3><?= $user['firstname'].' '.$user['lastname'] ?></h3>
			<hr>
			<form class="" action="<?=base_url('profile');?>" method="post">
				<div class="row">
					<?php if (session()->get('success')): ?>
						<div class="col-12">
							<div class="alert alert-success" role="alert">
								<?= session()->get('success') ?>
							</div>
						</div>
					<?php endif; ?>
					<?php if (isset($validation)): ?>
						<div class="col-12">
							<div class="alert alert-danger" role="alert">
								<?= $validation->listErrors() ?>
							</div>
						</div>
					<?php endif;?>
					<div class="col-12 col-sm-6">
						<div class="form-group">
						 <label for="firstname">Nombre</label>
						 <input type="text" class="form-control" name="firstname" id="firstname" value="<?= set_value('firstname', $user['firstname']) ?>">
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="form-group">
						 <label for="lastname">Apellido</label>
						 <input type="text" class="form-control" name="lastname" id="lastname" value="<?= set_value('lastname', $user['lastname']) ?>">
						</div>
					</div>
					<div class="col-12">
						<div class="form-group">
						 <label for="email">Email</label>
						 <input type="text" class="form-control" readonly id="email" value="<?= $user['email'] ?>">
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="form-group">
							 <label for="password">Constraseña</label>
							 <input type="password" class="form-control" name="password" id="password" value="">
						 </div>
					 </div>
					 <div class="col-12 col-sm-6">
						 <div class="form-group">
							<label for="password_confirm">Confirmar contraseña</label>
							<input type="password" class="form-control" name="password_confirm" id="password_confirm" value="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-sm-4">
						<button type="submit" class="btn btn-primary">
							Actualizar
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
