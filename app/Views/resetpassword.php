<?php
include 'templates/header.php';
?>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-sm-8 col-md-6 mt-5 pt-3 pb-3 bg-white from-wrapper border border-dark rounded">
			<?php
			if ($exist) {
			?>
				<h3>
					Reestableciento contraseña
				</h3>
				<hr>
				<p class="text-justify">
					Capture sus nuevos accesos
				</p>
				<?php if (session()->get('success')): ?>
					<div class="alert alert-success" role="alert">
					<?= session()->get('success') ?>
					</div>
				<?php endif; ?>
				<?php if (session()->get('danger')): ?>
					<div class="alert alert-danger" role="alert">
					<?= session()->get('danger') ?>
					</div>
				<?php endif; ?>
				<?php if (isset($validation)): ?>
					<div class="alert alert-danger" role="alert">
						<?= $validation->listErrors() ?>
					</div>
				<?php endif; ?>
				<form class="" action="<?=base_url('updatepassword?uuid='.$uuid);?>" method="post">
					<input type="hidden" name="id" id="id" value="<?=$id;?>" readonly="">
					<div class="form-group">
						 <label for="password">Contraseña</label>
						 <input type="password" class="form-control" name="password" id="password" value="" required="">
					 </div>
				
					 <div class="form-group">
						<label for="password_confirm">Confirmar contraseña</label>
						<input type="password" class="form-control" name="password_confirm" id="password_confirm" value="" required="">
					</div>
					<div class="row">
						<div class="col-12 col-sm-4">
							<button type="submit" class="btn btn-primary">Continuar</button>
						</div>
					</div>
				</form>
			<?php
			}else{
				if (!$reset) {
					echo '<p class="text-center">El enlace ha caducado o no existe, por favor rerifque, o genere uno nuevo. <a href="'.base_url('validate').'">Olvidé contraseña</a></p>';
				}
				if ($reset) {
					echo '<p class="text-center">Contraseña reestablecida correctamente, por favor inicie sesión. <a href="'.base_url('login').'">Iniciar sesión</a></p>';
				}
			}
			?>
		</div>
	</div>
</div>
<?php
include("templates/footer.php");
?>