<?php
include 'templates/header.php';
?>

<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-sm-8 col-md-6 mt-5 pt-3 pb-3 bg-white from-wrapper border border-dark rounded">
			<h3>Registro</h3>
			<hr>
			<form class="" action="<?=base_url('register');?>" method="post">
				<div class="row">
					<?php if (isset($validation)): ?>
						<div class="col-12">
							<div class="alert alert-danger" role="alert">
								<?= $validation->listErrors() ?>
							</div>
						</div>
					<?php endif; ?>
					<div class="col-12 col-sm-6">
						<div class="form-group">
						 <label for="firstname">Nombre</label>
						 <input type="text" class="form-control" name="firstname" id="firstname" value="<?= set_value('firstname') ?>" required="">
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="form-group">
						 <label for="lastname">Apellido</label>
						 <input type="text" class="form-control" name="lastname" id="lastname" value="<?= set_value('lastname') ?>">
						</div>
					</div>
					<div class="col-12">
						<div class="form-group">
						 <label for="email">Email</label>
						 <input type="email" class="form-control" name="email" id="email" value="<?= set_value('email') ?>" required="" autocomplete="off">
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<div class="form-group">
							 <label for="password">Contraseña</label>
							 <input type="password" class="form-control" name="password" id="password" value="" required="">
						 </div>
					 </div>
					 <div class="col-12 col-sm-6">
						 <div class="form-group">
							<label for="password_confirm">Confirmar contraseña</label>
							<input type="password" class="form-control" name="password_confirm" id="password_confirm" value="" required="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-sm-4">
						<button type="submit" class="btn btn-primary">Registrarse</button>
					</div>
					<div class="col-12 col-sm-8 text-right">
						<a href="<?=base_url('login');?>">Iniciar sesión</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
include("templates/footer.php");
?>