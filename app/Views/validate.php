<?php
include 'templates/header.php';
?>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-sm-8 col-md-6 mt-5 pt-3 pb-3 bg-white from-wrapper border border-dark rounded">
			<h3>Validar email</h3>
			<hr>
			<p class="text-justify">Si el email ingresado se encuentra registrado en nuestra base de datos, se enviará un enlace para reestablecer la contraseña.</p>
			<?php if (session()->get('success')): ?>
				<div class="alert alert-success" role="alert">
				<?= session()->get('success') ?>
				</div>
			<?php endif; ?>
			<?php if (session()->get('danger')): ?>
				<div class="alert alert-danger" role="alert">
				<?= session()->get('danger') ?>
				</div>
			<?php endif; ?>
			<form class="" action="<?=base_url('validate');?>" method="post">
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" class="form-control" name="email" id="email" value="" required="" autocomplete="off">
				</div>
				<div class="row">
					<div class="col-6">
						<button type="submit" class="btn btn-primary">Continuar</button>
					</div>
					<div class="col-6 text-right">
						<a href="<?=base_url('login');?>">Iniciar sesión</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
include("templates/footer.php");
?>