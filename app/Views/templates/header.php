<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="<?=base_url('public/assets/vendor/bootstrap/css/bootstrap.min.css');?>">
		<link rel="stylesheet" href="<?=base_url('public/assets/css/style.css');?>">
		<link rel="icon" href="<?=base_url('public/assets/images/favicon.png');?>">
		<title><?=$title;?></title>
	</head>
	<body>
		<?php
		$uri = service('uri');
		?>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="container">
				<a class="navbar-brand" href="<?=base_url('/');?>">
					linuxitos
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="30" height="30" focusable="false"><title>Menu</title><path stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" d="M4 7h22M4 15h22M4 23h22"></path></svg>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<?php if (session()->get('isLoggedIn')): ?>
						<div class="collapse navbar-collapse" id="navbarResponsive">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item dropdown desplegable">
									<a class="nav-link dropdown-toggle" href="#" id="dropdown06" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Menú
									</a>
									<div class="dropdown-menu dropdown-menu-right desplegable-menu border-dark" aria-labelledby="dropdown06">
										<a class="dropdown-item <?=($uri->getSegment(1) == 'dashboard' ? 'active' : null)?>" href="<?=base_url('dashboard');?>">Dashboard</a>
										<a class="dropdown-item <?=($uri->getSegment(1) == 'profile' ? 'active' : null)?>" href="<?=base_url('profile');?>">Perfil</a>
										<a class="dropdown-item <?=($uri->getSegment(1) == 'logout' ? 'active' : null)?>" href="<?=base_url('logout');?>">Salir</a>
									</div>
								</li>
							</ul>
						</div>
					<?php else: ?>
					<ul class="navbar-nav ml-auto">
						<li class="nav-item <?= ($uri->getSegment(1) == 'login' ? 'active' : null) ?>">
							<a class="nav-link" href="<?=base_url('login');?>">
								Iniciar sesión
							</a>
						</li>
						<li class="nav-item <?= ($uri->getSegment(1) == 'register' ? 'active' : null) ?>">
							<a class="nav-link" href="<?=base_url('register');?>">Registrarse</a>
						</li>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		</nav>
