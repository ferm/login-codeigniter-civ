# CodeIgniter 4: Inicio de sesión, registro y recuperación de contraseña

## Configuración inicial
- Fedora 33 x86_64 (podría funcionar en windows, no se ha probado)
- XAMPP 7.4.11
- PHP 7.4
- Codeigniter 4.0.4

## 1. Configuración
- La configuración requiere inicialmente modificar el archivo .env de acuerdoa a la configuración que se use en su equipo local.
	- Se configura la url de la aplicación
	- Accesos a la base de datos
	- Contraseñas y acceso al correo electrónico en caso de querer probar la funcionalidad 		de cambio de contraseña

## 2. Iniciando el proyecto
Abrir el navegador web y abrir la url del proyecto, si no cambió nada, sería http://localhost/devs/codeigniter-civ-login-auth

### 3. Capturas
![alt tag](./public/assets/images/1.png)
![alt tag](./public/assets/images/2.png)
![alt tag](./public/assets/images/3.png)
![alt tag](./public/assets/images/4.png)

Éste proyecto, es un fork del proyecto https://github.com/alexlancer/codeigniter4login, sin embargo dicho proyecto no contaba con el módulo de cambio de contraseña, por lo tanto lo añadí, y quise publicarlo de nuevo.
